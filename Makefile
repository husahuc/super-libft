NAME = libft.a

DARK_BLUE = \033[1;38;5;110m
GREEN = \033[1;32;111m
RED = \033[1;31m
COMPILER = clang
# flags

FLAGS = -Wall -Werror -Wextra -O3 -g3 --pedantic

# PATH

SRC_PATH = ./src/
OBJ_PATH = ./obj/
INC_PATH = ./includes/

STR_SRC_PATH = $(addprefix $(SRC_PATH), str/)
STR_OBJ_PATH = $(addprefix $(OBJ_PATH), str/)

UNIX_SRC_PATH = $(addprefix $(SRC_PATH), unix/)
UNIC_OBJ_PATH = $(addprefix $(OBJ_PATH), unix/)

INT_SRC_PATH = $(addprefix $(SRC_PATH), int/)
INT_OBJ_PATH = $(addprefix $(OBJ_PATH), int/)

LST_SRC_PATH = $(addprefix $(SRC_PATH), lst/)
LST_OBJ_PATH = $(addprefix $(OBJ_PATH), lst/)

MEM_SRC_PATH = $(addprefix $(SRC_PATH), mem/)
MEM_OBJ_PATH = $(addprefix $(OBJ_PATH), mem/)

GNL_SRC_PATH = $(addprefix $(SRC_PATH), GNL/)
GNL_OBJ_PATH = $(addprefix $(OBJ_PATH), GNL/)

MATH_SRC_PATH = $(addprefix $(SRC_PATH), math/)
MATH_OBJ_PATH = $(addprefix $(OBJ_PATH), math/)

TAB_SRC_PATH = $(addprefix $(SRC_PATH), tab/)
TAB_OBJ_PATH = $(addprefix $(OBJ_PATH), tab/)

POINT_SRC_PATH = $(addprefix $(SRC_PATH), point/)
POINT_OBJ_PATH = $(addprefix $(OBJ_PATH), point/)

PRINTF_SRC_PATH = $(addprefix $(SRC_PATH), mini_printf/)
PRINTF_OBJ_PATH = $(addprefix $(OBJ_PATH), mini_printf/)

# SRC

STR_INC_NAME = str.h
STR_PUT_NAME =	ft_putchar.c ft_putchar_fd.c ft_putendl.c ft_putendl_fd.c \
				ft_putstr.c ft_putstr_fd.c ft_putnbr.c ft_putnbr_fd.c
STR_TEST_NAME = ft_strlen.c ft_count_block.c ft_count_word.c ft_strcmp.c ft_strncmp.c \
				ft_strcchr.c ft_strrchr.c ft_strequ.c ft_strnequ.c ft_strstr.c \
				ft_strchr.c
STR_JOIN_NAME = ft_strjoin.c ft_strjoinf.c ft_strjoin_free.c ft_strsplit.c ft_strsub.c ft_strtrim.c ft_strncpy.c \
				ft_strcat.c ft_strncat.c ft_strlcat.c ft_strnstr.c
STR_NEW_NAME = ft_strcpy.c ft_strdup.c ft_strnew.c
STR_MODF_NAME = ft_strcapitalize.c ft_strlowcase.c ft_strupcase.c
STR_FUN_NAME = ft_striter.c ft_striteri.c ft_strmap.c ft_strmapi.c ft_sort_list_world.c
STR_FREE_NAME = ft_bzero.c ft_strclr.c ft_strdel.c
STR_CHAR_NAME = ft_isalpha.c ft_isascii.c  ft_isdigit.c ft_isprint.c \
				ft_isalnum.c ft_isalnum_underscore.c ft_tolower.c ft_toupper.c

STR_SRC_NAME = $(addprefix put/, $(STR_PUT_NAME)) $(addprefix test/, $(STR_TEST_NAME)) \
			$(addprefix split_join/, $(STR_JOIN_NAME)) $(addprefix new/, $(STR_NEW_NAME)) \
			$(addprefix modify/, $(STR_MODF_NAME)) $(addprefix function/, $(STR_FUN_NAME)) \
			$(addprefix free/, $(STR_FREE_NAME)) $(addprefix char/, $(STR_CHAR_NAME))

INT_INC_NAME = int.h
INT_SRC_NAME = ft_atoi.c ft_count_len.c ft_itoa.c

GNL_INC_NAME = get_next_line.h
GNL_SRC_NAME = get_next_line.c

MEM_INC_NAME = mem.h
MEM_SRC_NAME = ft_memalloc.c ft_memcpy.c ft_memchr.c ft_memccpy.c ft_memdel.c \
				ft_memmove.c ft_memset.c ft_realloc.c

TAB_INC_NAME = tab.h
TAB_SRC_NAME = ft_tabfree.c ft_free_tab.c ft_tablen.c ft_print_tab.c \
				ft_quicksort.c

LST_INC_NAME = lst.h
LST_SRC_NAME = ft_lstadd.c ft_lstnew.c ft_lstdelone.c ft_lstdel.c ft_lstiter.c \
				ft_lstmap.c

PRINTF_INC_NAME = mini_printf.h
PRINTF_SRC_NAME = ft_printf_err.c mini_printf.c mini_printf_type.c

UNIX_INC_NAME = unix.h
UNIX_SRC_NAME = ft_options.c

# var

STR_OBJ_NAME = $(STR_SRC_NAME:.c=.o)
STR_OBJ = $(addprefix $(STR_OBJ_PATH), $(STR_OBJ_NAME))
STR_INC = $(addprefix $(INC_PATH), $(STR_INC_NAME))

INT_OBJ_NAME = $(INT_SRC_NAME:.c=.o)
INT_OBJ = $(addprefix $(INT_OBJ_PATH), $(INT_OBJ_NAME))
INT_INC = $(addprefix $(INC_PATH), $(INT_INC_NAME))

GNL_OBJ_NAME = $(GNL_SRC_NAME:.c=.o)
GNL_OBJ = $(addprefix $(GNL_OBJ_PATH), $(GNL_OBJ_NAME))
GNL_INC = $(addprefix $(INC_PATH), $(GNL_INC_NAME))

MEM_OBJ_NAME = $(MEM_SRC_NAME:.c=.o)
MEM_OBJ = $(addprefix $(MEM_OBJ_PATH), $(MEM_OBJ_NAME))
MEM_INC = $(addprefix $(INC_PATH), $(MEM_INC_NAME))

TAB_OBJ_NAME = $(TAB_SRC_NAME:.c=.o)
TAB_OBJ = $(addprefix $(TAB_OBJ_PATH), $(TAB_OBJ_NAME))
TAB_INC = $(addprefix $(INC_PATH), $(TAB_INC_NAME))

LST_OBJ_NAME = $(LST_SRC_NAME:.c=.o)
LST_OBJ = $(addprefix $(LST_OBJ_PATH), $(LST_OBJ_NAME))
LST_INC = $(addprefix $(INC_PATH), $(LST_INC_NAME))

PRINTF_OBJ_NAME = $(PRINTF_SRC_NAME:.c=.o)
PRINTF_OBJ = $(addprefix $(PRINTF_OBJ_PATH), $(PRINTF_OBJ_NAME))
PRINTF_INC = $(addprefix $(INC_PATH), $(PRINTF_INC_NAME))

UNIX_OBJ_NAME = $(UNIX_SRC_NAME:.c=.o)
UNIX_OBJ = $(addprefix $(UNIX_OBJ_PATH), $(UNIX_OBJ_NAME))
UNIX_INC = $(addprefix $(INC_PATH), $(UNIX_INC_NAME))

ALL_OBJ = $(STR_OBJ) $(INT_OBJ) $(GNL_OBJ) $(MEM_OBJ) $(TAB_OBJ) $(LST_OBJ) \
		$(PRINTF_OBJ) $(UNIX_OBJ)

# Test

TEST_PATH = ./Test/

TEST_UNIX = test_options
TEST_UNIX_FILE = test_options.c
TEST_UNIX_PATH = $(addprefix $(TEST_PATH), $(TEST_UNIX_FILE))

TEST_QUICKSORT = test_quicksort
TEST_QUICKSORT_FILE = test_quicksort.c
TEST_QUICKSORT_PATH = $(addprefix $(TEST_PATH), $(TEST_QUICKSORT_FILE))

TEST_ALL = $(TEST_UNIX) $(TEST_QUICKSORT)

# Rules

all : $(NAME)

$(NAME): $(ALL_OBJ)
	@ar rcs $(NAME) $(ALL_OBJ)
	@echo "\033[2K\r$(DARK_BLUE)libft:	$(GREEN)loaded\033[0m"

$(STR_OBJ_PATH)%.o: $(STR_SRC_PATH)%.c $(STR_INC)
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	@$(COMPILER) $(FLAGS) -I $(INC_PATH) -o $@ -c $<
	@printf "\033[2K\r$(DARK_BLUE)libft str:	\033[37m$<\033[36m \033[0m"

$(INT_OBJ_PATH)%.o: $(INT_SRC_PATH)%.c $(INT_INC)
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	@$(COMPILER) $(FLAGS) -I $(INC_PATH) -o $@ -c $<
	@printf "\033[2K\r$(DARK_BLUE)libft int:	\033[37m$<\033[36m \033[0m"

$(GNL_OBJ_PATH)%.o: $(GNL_SRC_PATH)%.c $(GNL_INC)
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	@$(COMPILER) $(FLAGS) -I $(INC_PATH) -o $@ -c $<
	@printf "\033[2K\r$(DARK_BLUE)libft GNL:	\033[37m$<\033[36m \033[0m"

$(MEM_OBJ_PATH)%.o: $(MEM_SRC_PATH)%.c $(MEM_INC)
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	@$(COMPILER) $(FLAGS) -I $(INC_PATH) -o $@ -c $<
	@printf "\033[2K\r$(DARK_BLUE)libft mem:	\033[37m$<\033[36m \033[0m"

$(TAB_OBJ_PATH)%.o: $(TAB_SRC_PATH)%.c $(TAB_INC)
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	@$(COMPILER) $(FLAGS) -I $(INC_PATH) -o $@ -c $<
	@printf "\033[2K\r$(DARK_BLUE)libft tab:	\033[37m$<\033[36m \033[0m"

$(LST_OBJ_PATH)%.o: $(LST_SRC_PATH)%.c $(LST_INC)
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	@$(COMPILER) $(FLAGS) -I $(INC_PATH) -o $@ -c $<
	@printf "\033[2K\r$(DARK_BLUE)libft lst:	\033[37m$<\033[36m \033[0m"

$(PRINTF_OBJ_PATH)%.o: $(PRINTF_SRC_PATH)%.c $(PRINTF_INC)
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	@$(COMPILER) $(FLAGS) -I $(INC_PATH) -o $@ -c $<
	@printf "\033[2K\r$(DARK_BLUE)libft printf:	\033[37m$<\033[36m \033[0m"

$(UNIX_OBJ_PATH)%.o: $(UNIX_SRC_PATH)%.c $(UNIX_INC)
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	@$(COMPILER) $(FLAGS) -I $(INC_PATH) -o $@ -c $<
	@printf "\033[2K\r$(DARK_BLUE)libft unix:	\033[37m$<\033[36m \033[0m"

clean:
	@rm -rf $(OBJ_PATH)
	@echo "\033[2K\r$(DARK_BLUE)libft:	$(RED)objet files deleted\033[0m"

fclean: clean
	@rm -rf $(NAME)
	@echo "\033[2K\r$(DARK_BLUE)libft:	$(RED)libft.a deleted\033[0m"

re: fclean all

# Test rules

$(TEST_UNIX): all $(TEST_UNIX_PATH)
	$(COMPILER) $(TEST_UNIX_PATH) $(NAME) -o $@

$(TEST_QUICKSORT): all $(TEST_QUICKSORT_PATH)
	$(COMPILER) $(TEST_QUICKSORT_PATH) $(NAME) -o $@

clean_test:
	rm -rf $(TEST_ALL)
