/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   test_options.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/12 13:34:18 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/11/29 14:57:22 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../includes/unix.h"
#include "stdio.h"

unsigned long	char_to_bin10(char ch)
{
	unsigned char uch;
	unsigned long sum;
	unsigned long power;

	uch = ch;
	sum = 0;
	power = 1;
	while (uch)
	{
		if (uch & 1)
		{
			sum += power;
		}
		power *= 10;
		uch /= 2;
	}
	return (sum);
}

int				main(int argc, char *argv[])
{
	ssize_t op;

	op = ft_options(argv, "abc");
	printf("%zd\n", op);
	printf("%lu", char_to_bin10(op));
	return (0);
}
