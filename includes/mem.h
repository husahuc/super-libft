/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mem.h                                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/28 15:12:25 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/11/28 15:16:04 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef MEM_H
# define MEM_H

# include <unistd.h>
# include <stdlib.h>

void		*ft_memalloc(size_t size);

void		*ft_memcpy(void *str1, const void *str2, size_t n);
void		*ft_memccpy(void *dest, const void *src, int c, size_t n);

void		*ft_memchr(const void *str, int c, size_t n);

int			ft_memcmp(const void *str1, const void *str2, size_t n);
void		ft_memdel(void **ap);

void		*ft_memmove(void *dest, const void *src, size_t len);

void		*ft_memset(void *str, int c, size_t n);
void		*ft_realloc(void *ptr, size_t size);
#endif
