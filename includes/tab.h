/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   tab.h                                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/28 15:20:30 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/11/29 14:55:20 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef TAB_H
# define TAB_H

void		ft_tabfree(char **res);

void		ft_free_tab(char **tabl);

int			ft_tabclen(char **array);

void		ft_print_tab(char **res);

void        ft_quicksort(int arr[], int low, int hight);
#endif
