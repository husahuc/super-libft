/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   int.h                                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/28 15:06:44 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/11/28 15:11:09 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef INT_H
# define INT_H

int			ft_atoi(const char *str);
int			ft_count_len(int nb);
char		*ft_itoa(int n);

#endif
