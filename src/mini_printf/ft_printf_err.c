/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_printf_err.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/29 11:13:29 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/10/29 11:13:31 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "mini_printf.h"

int			ft_printf_err(const char *str, ...)
{
	va_list	params;

	va_start(params, str);
	mini_printf_options(str, params, 2);
	va_end(params);
	return (0);
}
