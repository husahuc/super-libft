/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_quicksort.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/29 14:26:30 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/11/29 15:11:30 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "tab.h"

void	swap(int *a, int *b)
{
	int t;

	t = *a;
	*a = *b;
	*b = t;
}

/*
** test for int tab
*/

int		partition(int arr[], int low, int hight)
{
	int pivot;
	int i;
	int j;

	pivot = arr[hight];
	i = low - 1;
	j = low;
	while (j <= hight - 1)
	{
		if (arr[j] < pivot)
		{
			i++;
			swap(&arr[i], &arr[j]);
		}
		j++;
	}
	swap(&arr[i + 1], &arr[hight]);
	return (i + 1);
}

void	ft_quicksort(int arr[], int low, int hight)
{
	int pi;

	if (low < hight)
	{
		pi = partition(arr, low, hight);
		ft_quicksort(arr, low, pi - 1);
		ft_quicksort(arr, pi + 1, hight);
	}
}
