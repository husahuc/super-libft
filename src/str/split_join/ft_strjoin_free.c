/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strjoin_free.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/29 10:18:47 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/10/29 10:18:49 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "str.h"

void	ft_strjoin_free(char **s1, const char *s2)
{
	char *ret;

	if (!(ret = ft_strjoin(*s1, s2)))
	{
		ft_strdel(s1);
		return ;
	}
	ft_strdel(s1);
	*s1 = ret;
}
