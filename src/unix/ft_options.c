/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_options.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/12 13:14:24 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/11/21 11:01:57 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "unix.h"
#include "str.h"

static ssize_t	option_binary(char c, char *letters, ssize_t options)
{
	int i;

	i = -1;
	while (letters[++i])
	{
		if (letters[i] == c)
			break ;
	}
	options = options | 1 << i;
	return (options);
}

ssize_t			ft_options(char **str, char *letters)
{
	int		i;
	int		j;
	ssize_t	options;
	char	current;

	i = 0;
	options = 0;
	while (str[++i])
	{
		if (str[i][0] != '-')
			break ;
		j = 1;
		while (str[i][j])
		{
			if ((current = ft_strcchr(letters, str[i][j])) == 0)
				return (-1);
			options = option_binary(current, letters, options);
			j++;
		}
	}
	return (options);
}
